/**
 * @file
 * Define tags as "block level" so the editor doesn't try to put paragraph tags around them.
 */

cktags = ['ckpowerbi'];

// Global variable used to store fakeImage element when socialmedia is double clicked.
var doubleclick_element;

CKEDITOR.plugins.add('embedded_powerbi', {
  requires : ['dialog', 'fakeobjects'],
  init: function (editor) {

    // Define plugin name.
    var pluginName = 'embedded_powerbi';

    // Set up object to share variables amongst scripts.
    CKEDITOR.embedded_powerbi = {};

    // Register button for Power BI.
    editor.ui.addButton('Power BI', {
      label : "Add/Edit Power BI",
      command : 'powerbi',
      icon: this.path + 'icons/power_bi.png',
    });

    // Register right-click menu item for Power BI.
    editor.addMenuItems({
      powerbi : {
        label : "Edit Power BI",
        icon: this.path + 'icons/power_bi.png',
        command : 'powerbi',
        group : 'image',
        order : 1
      }
    });

    // Make ckpowerbi to be a self-closing tag.
    CKEDITOR.dtd.$empty['ckpowerbi'] = 1;

    // Make sure the fake element for Power BI has a name.
    CKEDITOR.embedded_powerbi.ckpowerbi = 'powerbi';
    CKEDITOR.lang.en.fakeobjects.ckpowerbi = CKEDITOR.embedded_powerbi.ckpowerbi;

    // Add JavaScript file that defines the dialog box for Power BI.
    CKEDITOR.dialog.add('powerbi', this.path + 'dialogs/powerbi.js');

    // Register command to open dialog box when button is clicked.
    editor.addCommand('powerbi', new CKEDITOR.dialogCommand('powerbi'));

    // Regular expressions for Power BI.
    CKEDITOR.embedded_powerbi.powerbi_regex = /https\:\/\/app.powerbi.com\/reportEmbed\?reportId\=/;

    // Open the appropriate dialog box if an element is double-clicked.
    editor.on('doubleclick', function (evt) {

      var element = evt.data.element;

      // Store the element as global variable to be used in onShow of dialog, when double clicked.
      doubleclick_element = element;

      if (element.is('img') && element.data('cke-real-element-type') === 'ckpowerbi') {

        evt.data.dialog = 'powerbi';
      }
    });

    // Add the appropriate right-click menu item if an element is right-clicked.
    if (editor.contextMenu) {

      editor.contextMenu.addListener(function (element, selection) {

        if (element && element.is('img') && element.data('cke-real-element-type') === 'ckpowerbi') {

          return { powerbi : CKEDITOR.TRISTATE_OFF };
        }
      });
    }

    // Add CSS to use in-editor to style custom (fake) elements.
    CKEDITOR.addCss(

      'img.ckpowerbi {' +
      'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/power_bi_big.png') + ');' +
      'background-position: center center;' +
      'background-repeat: no-repeat;' +
      'background-color: #000;' +
      'width: 100%;' +
      'height: 100px;' +
      'margin: 0 0 10px 0;' +
      'margin-left: auto;' +
      'margin-right: auto;' +
      '}' +

      // Power BI: Definitions for wide width.
      '.uw_tf_standard_wide p img.ckpowerbi {' +
      'height: 200px;' +
      '}' +

      // Power BI: Definitions for standard width.
      '.uw_tf_standard p img.powerbi {' +
      'height: 200px;' +
      '}'
    );
  },
  afterInit : function (editor) {

    // Make fake image display on first load/return from source view.
    if (editor.dataProcessor.dataFilter) {
      editor.dataProcessor.dataFilter.addRules({
        elements : {
          ckpowerbi : function (element) {

            // Reset title.
            CKEDITOR.lang.en.fakeobjects.ckpowerbi = CKEDITOR.embedded_powerbi.ckpowerbi;

            // Adjust title if a list name is present.
            if (element.attributes['data-powerbi-id']) {
              CKEDITOR.lang.en.fakeobjects.ckpowerbi = 'Power BI: ' + element.attributes['data-powerbi-id'];
            }

            // Note that this just accepts whatever attributes are on the element; may want to filter these.
            return editor.createFakeParserElement(element, 'ckpowerbi', 'ckpowerbi', false);
          }
        }
      });
    }
  }
});
