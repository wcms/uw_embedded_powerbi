/**
 * @file
 */

(function () {
  // Variable used to track when the form is submitted.
  // i.e to check for double submissions on using enter key in dialog.
  var formSubmitted = false;
  var powerbiDialog = function (editor) {
    return {
      title : 'Power BI Properties',
      minWidth : 625,
      minHeight : 150,
      contents: [{
        id: 'powerbi',
        label: 'powerbi',
        elements:[{
          type: 'text',
          id: 'powerbi-id',
          label: 'Please enter a Power BI Embed Report URL:',
          required: true,
          setup: function (element) {
            this.setValue(element.getAttribute('data-powerbi-id'));
          }
        }]
      }],
      onOk: function () {
        // Get form information.
        powerbi_id = this.getValueOf('powerbi','powerbi-id');

        // Validate input. Note that there is probably a CKEditor specific way to do this, but this works.
        errors = "";
        if (!CKEDITOR.embedded_powerbi.powerbi_regex.test(powerbi_id)) {
          errors += "You must enter a valid Power BI Report URL.\r\n";
        }
        else {
          errors = '';
        }
        if (!powerbi_id) {
          errors = "You must enter a Power BI Embed Report URL.\r\n";
        }

        // Ensure that form has not been submitted before.
        if (formSubmitted == true) {
          formSubmitted = false;
          return false;
        }
        // Only display errors if there are errors to display and the form has not been run before.
        else if (errors && formSubmitted == false) {
          alert(errors);
          formSubmitted = true;
          return false;
        }
        else {
          // Create the Power BI element.
          var ckpowerbiNode = new CKEDITOR.dom.element('ckpowerbi');

          // Save contents of dialog as attributes of the element.
          ckpowerbiNode.setAttribute('data-powerbi-id', powerbi_id);

          // Adjust title based on user input.
          CKEDITOR.lang.en.fakeobjects.ckpowerbi = 'Power BI: ' + powerbi_id;
          // Create the fake image for this element and insert into the document (realElement, className, realElementType, isResizable)
          var newFakeImage = editor.createFakeElement(ckpowerbiNode, 'ckpowerbi', 'ckpowerbi', false);
          // Set the fake object to the entered height; if there isn't one, use 100 so it's not invisible.
          newFakeImage.addClass('ckpowerbi');
          if (this.fakeImage) {
            newFakeImage.replace(this.fakeImage);
            editor.getSelection().selectElement(newFakeImage);
          }
          else {
            editor.insertElement(newFakeImage);
          }
          // Reset title.
          CKEDITOR.lang.en.fakeobjects.ckpowerbi = CKEDITOR.embedded_powerbi.ckpowerbi;
        }
      },
      onShow: function () {
        // Set up to handle existing items.
        this.fakeImage = this.ckpowerbiNode = null;
        var fakeImage;

        // Check if element is right clicked or icon was clicked, if not use global varaible doubleclick_element.
        if (this.getSelectedElement()) {
          fakeImage = this.getSelectedElement();
        }
        else {
          fakeImage = fakeImage;
        }

        if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'ckpowerbi') {
          this.fakeImage = fakeImage;
          var ckpowerbiNode = editor.restoreRealElement(fakeImage);
          this.ckpowerbiNode = ckpowerbiNode;
          this.setupContent(ckpowerbiNode);
        }
      }
    }
  }

  CKEDITOR.dialog.add('powerbi', function (editor) {
    return powerbiDialog(editor);
  });
})();
